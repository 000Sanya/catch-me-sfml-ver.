﻿module Game;

import std.stdio, std.datetime : StopWatch;
public import ScreenManager, Screen;
import dsfml.audio, dsfml.graphics, dsfml.system, dsfml.window;

class Game
{
	RenderWindow win;
	ScreenManager screens;
	Clock clock;

	protected
	{
		Time total, elapsed;
	}

	this(uint width, uint height, string title)
	{
		win = new RenderWindow(VideoMode(width, height), title, Window.Style.DefaultStyle);
		win.setVerticalSyncEnabled(true);
		screens = new ScreenManager(this);
		clock = new Clock();
	}

	void run()
	{
		initialization();
		//loadContent();
		loop();
	}

protected:
	void initialization(){}
	void update(in Time total, in Time elapsed){}
	void draw(RenderTarget render, in Time total, in Time elapsed){}

private:
	void loop()
	{
		StopWatch _stop;
		while(win.isOpen)
		{
			Event event;
			while(win.pollEvent(event))
			{
				alias Type = Event.EventType;
				switch(event.type)
				{
					case Type.Closed:
						win.close;
						break;
					default:
						break;
				}
			}

			auto prevTotal = total;
			total = clock.getElapsedTime();
			elapsed = total - prevTotal;

			screens.update(total, elapsed);

			update(this.total, this.elapsed);

			win.clear();

			screens.draw(win, total, elapsed);

			draw(this.win, this.total, this.elapsed);

			win.display();
		}
  
	}
}

