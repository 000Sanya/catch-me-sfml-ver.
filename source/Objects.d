﻿module Objects;

import std.stdio;
import app;
import dsfml.audio, dsfml.graphics, dsfml.system, dsfml.window;

class GameObject
{
	Sprite sprite;

	this(Texture tex)
	{
		this.sprite = new Sprite(tex);
	}

	void move(Vector2f distance)
	{
		sprite.position = sprite.position + distance;
	}

	void move(float x, float y)
	{
		sprite.position = sprite.position + Vector2f(x, y);
	}
}

class Platform : GameObject
{
	float velocity = .7f;
	alias move = GameObject.move;

	this(Texture tex)
	{
		super(tex);
	}

	enum Direction : int
	{
		Left = -1,
		Right = 1
	}

	enum CheckResult
	{
		Error = -1,
		None,
		Missed,
		Сaught
	}

	void move(Direction dir, int elapsed, float vel)
	{
		int x = cast(int)(elapsed * velocity * dir);
		sprite.position = sprite.position + Vector2f(x, 0);
		auto size = sprite.getTexture().getSize();

		if(sprite.position.x + size.x > GFWidth) sprite.position = Vector2f(GFWidth - size.x, sprite.position.y);
		if(sprite.position.x < 0) sprite.position = Vector2f(0, sprite.position.y);
	}

	byte check(Enemy obj)
	{
		Vector2f pl = this.sprite.position;
		Vector2f ob = obj.sprite.position;
		FloatRect oSize = obj.sprite.getLocalBounds();
		FloatRect pSize = sprite.getLocalBounds();
		if(ob.y < GFHeight - 30) return CheckResult.None;

		bool x = ob.x >= pl.x - (oSize.width / 2) &&
			ob.x <= pl.x + pSize.width + (oSize.width / 2);
		bool y = ob.y + oSize.height >= GFHeight - pSize.height;
		if(x && y) return CheckResult.Сaught;
		else if(!x && y) return CheckResult.Missed;
		return CheckResult.Error;
	}
}

class Enemy : GameObject
{
	private float* _velocity;
	protected int score;

	this(Texture tex, ref float velocity, float x)
	{
		_velocity = &velocity;
		score = 100;
		super(tex);
		super.move(x, 0);
	}

	void move(int elapsed)
	{
		sprite.position = sprite.position + Vector2f(0, (*_velocity) * elapsed);
	}

	int getScore() const @property
	{
		return score;
	}
}