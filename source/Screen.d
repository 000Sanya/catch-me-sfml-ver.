﻿module Screen;

abstract class Screen
{
	import ScreenManager, dsfml.window, dsfml.graphics;

	protected ScreenManager _screenManager;

	void update(in Time total, in Time elapsed){}
	void draw(RenderTarget render, in Time total, in Time elapsed){}

	final ref ScreenManager screenManager() @property
	{
		return _screenManager;
	}
}

