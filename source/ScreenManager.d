﻿module ScreenManager;

import dsfml.system, dsfml.graphics;

class ScreenManager
{
	import Game, Screen;

	private Game _game;
	private Screen[string] _screens;
	private Screen* _active;

	this(Game game)
	{
		this._game = game;
	}

	void add(string name, Screen value)
	{
		//value.screenManager = this;
		_screens[name] = value;
	}

	bool activate(string name)
	{
		if(Screen* scr = name in _screens)
		{
			_active = scr;
			return true;
		}
		return false;
	}

	void update(in Time total, in Time elapsed)
	{
		if((*_active) !is null)
			_active.update(total, elapsed);
	}

	void draw(RenderTarget render, in Time total, in Time elapsed)
	{
		if((*_active) !is null)
			_active.draw(render, total, elapsed);	
	}
}

