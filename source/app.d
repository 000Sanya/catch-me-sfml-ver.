import std.stdio, std.container, std.random, std.range, std.string, std.conv;

import Game, Objects;
import dsfml.audio, dsfml.graphics, dsfml.system, dsfml.window;

Font font;

Texture tPlatform, enemy, bg;
enum
{
	ScreenWidth = 640,
	ScreenHeight = 480,
	GFWidth = 480,
	GFHeight = 480
}

void main()
{
	font = new Font();
	font.loadFromFile("../fonts/arial.ttf");
	tPlatform = new Texture();
	tPlatform.loadFromFile("../textures/platform.png");
	enemy = new Texture();
	enemy.loadFromFile("../textures/enemy.png");
	bg = new Texture();
	bg.loadFromFile("../textures/bg.png");
	auto game = new CatchMeGame();
	game.run();
}

class CatchMeGame : Game
{
	this()
	{
		super(640, 480, "Catch Me!");
	}

	override void initialization() 
	{
		screens.add("Main", new GameScreen());
		screens.activate("Main");
		super.initialization();
	}
	
}

class MenuScreen : Screen
{
	Text click;
	Sprite logo;
	bool isDraw = true;
	private Texture _logo;

	this()
	{
		click = new Text("Press Space to start game", font, 14);
		click.setColor(Color.White);
		_logo = new Texture();
		_logo.loadFromFile("../textures/logo.png");
		logo = new Sprite(_logo);
		{
			Vector2u size = _logo.getSize;
			auto pos = Vector2f
				(
					ScreenWidth / 2 - size.x / 2,
					ScreenHeight / 2 - size.y / 2 - 60,
				);
			logo.position(pos);
		}
		{
			FloatRect size = click.getLocalBounds;
			auto pos = Vector2f
				(
					ScreenWidth / 2 - size.width / 2,
					ScreenHeight / 2 - size.height / 2,
					);
			click.position(pos);
		}
	}

	override void update(in Time total,in Time elapsed) 
	{
		static int counter;
		if(counter >= 500)
		{
			isDraw = !isDraw;
			counter = 0;
		}
		if(Keyboard.isKeyPressed(Keyboard.Key.Space))
			return;
		counter += elapsed.asMilliseconds();
		super.update(total,elapsed);
	}
	

	override void draw(RenderTarget render,in Time total,in Time elapsed) 
	{
		render.draw(logo);
		if(isDraw)
			render.draw(click);
		super.draw(render,total,elapsed);
	}
	
}

class GameScreen : Screen
{
	Platform platform;
	DList!Enemy enemies;
	Sprite background;
	Text scoreBoard;

	private
	{
		float _enemyVelocity = 0.3f;;
		int score, caught, missed, combo;
		int respawnTime = 1000;
	}

	this()
	{
		background = new Sprite(bg);
		platform = new Platform(tPlatform);
		platform.move
			(
				GFWidth / 2 - tPlatform.getSize.x / 2,
				GFHeight - tPlatform.getSize.y
			);
		scoreBoard = new Text(null, font, 16);
		scoreBoard.position = Vector2f(GFWidth + 10, 10);
		enemies ~= new Enemy(enemy, _enemyVelocity, uniform(0, GFWidth - enemy.getSize().x));
	}

	override void update(in Time total,in Time elapsed) 
	{
		static int counter;

		if(Keyboard.isKeyPressed(Keyboard.Key.Left))
			platform.move(Platform.Direction.Left, elapsed.asMilliseconds, 0f);
		else if(Keyboard.isKeyPressed(Keyboard.Key.Right))
			platform.move(Platform.Direction.Right, elapsed.asMilliseconds, 0f);

		if(counter >= respawnTime)
		{
			enemies ~= new Enemy(enemy, _enemyVelocity, uniform(0, GFWidth - enemy.getSize().x));
			counter = 0;
		}
		counter += elapsed.asMilliseconds;

		auto range = enemies[];
		foreach(Enemy en; range)
		{
			en.move(elapsed.asMilliseconds);
		}

		alias CheckResult = Platform.CheckResult;

		if(!enemies.empty)
		{
			auto en = enemies.front;
			byte res = platform.check(en);
			
			if(res == CheckResult.Сaught)
			{
				++caught;
				++combo;
				score += en.getScore * combo * (1 + _enemyVelocity);
				enemies.removeFront();
			}
			else if(res == CheckResult.Missed)
			{
				++missed;
				combo = 0;
				enemies.removeFront();
			}
		}

		string temp = format("Счет: %s\nПоймано: %s\nПропущено: %s", score, caught, missed);
		scoreBoard.setString(to!dstring(temp));

		super.update(total,elapsed);
	}
	

	override void draw(RenderTarget render,in Time total,in Time elapsed) 
	{
		render.draw(background);
		auto range = enemies[];
		foreach(en; range)
		{
			render.draw(en.sprite);
		}
		render.draw(platform.sprite);
		render.draw(scoreBoard);
		super.draw(render,total,elapsed);
	}
	
}
